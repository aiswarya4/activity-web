<?php

use com\linways\ams\addon\common\Constants;
use com\linways\nucleus\core\service\AddonService;

try{
  $fileIterator = new \RecursiveIteratorIterator(
      new \RecursiveDirectoryIterator($addon_src_root),
      \RecursiveIteratorIterator::SELF_FIRST);

  // START: To load enabled addon list

  $collegeCode = $GLOBALS['COLLEGE_CODE'];
  $scopeName = Constants::SCOPE_INSTITUTION;
  $enabledAddons =[];
  $addons =  AddonService::getInstance()->getAddonsEnabled($collegeCode,$scopeName);
  foreach ($addons as $object)
      $enabledAddons[] = $object->addon->packageName;

  // END: To load enabled addon list
  foreach($fileIterator as $file) {
      if($file->isFile() &&endsWith($file->getRealPath(), 'Hook.php')) {
          foreach($enabledAddons as $addon){
              if(strpos($file->getRealPath(),'/'. $addon .'/') !== false){
                  include_once $file->getRealpath();
              }
          }
      }
  }
}catch(Exception $e){
    error_log($e->getMessage());
  // Fail silently
}


// CommonUtils::endsWith cannot be used here since this file is autoloaded on dependent projects
function endsWith($haystack, $needle)
{
  $length = strlen($needle);
  return $length === 0 ||
  (substr($haystack, -$length) === $needle);
}
