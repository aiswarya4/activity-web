<?php

$timeAgoFilter = new Twig_SimpleFilter('timeago', function ($datetime) {

    $time = time() - strtotime($datetime);

    $units = array(
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($units as $unit => $val) {
        if ($time < $unit)
            continue;
        $numberOfUnits = floor($time / $unit);
        return ($val == 'second') ? 'a few seconds ago' : (($numberOfUnits > 1) ? $numberOfUnits : 'a') . ' ' . $val . (($numberOfUnits > 1) ? 's' : '') . ' ago';
    }
});

$twig->addFilter($timeAgoFilter);


$addPositionFilter = new Twig_SimpleFilter('addPosition', function ($number) {
    if ($number % 10 == 1 && $number != 11)
        return 'st';
    elseif ($number % 10 == 2 && $number != 12)
        return 'nd';
    elseif ($number % 10 == 3 && $number != 13)
        return 'rd';
    return 'th';
});

$twig->addFilter($addPositionFilter);


$userHasPermissionFunction = new Twig_SimpleFunction('userHasPermission', function ($permissions) {
    // return PermissionService::getInstance()->staffHasPermission($GLOBALS['userId'], $permissions);
    return true;
});

$twig->addFunction($userHasPermissionFunction);

$twig->addGlobal('session', $_SESSION);

$isCurrentRoute = new Twig_SimpleFilter('is_current_route', function ($routeName) {
    global $app;
    $urlOfRouteName = $app->getContainer()
        ->get('router')
        ->pathFor($routeName);
    $request = $app->getContainer()->get('request');
    $currentUrl = $request->getUri()->getBasePath() . "/" . $request->getUri()->getPath();
    if ($currentUrl == $urlOfRouteName) {
        return true;
    }
    return false;
});

$twig->addFilter($isCurrentRoute);

$getEnv= new Twig_SimpleFunction('getenv', function ($envVar) {
    return getenv($envVar);
});



?>
