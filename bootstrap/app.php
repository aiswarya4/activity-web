<?php
ob_clean();

if(ini_get('zlib.output_compression') === "1"){
    header('Content-Encoding: gzip');
}

require '../vendor/autoload.php';

session_start();

// Constant refers to the base source folder
define('SOURCE_DIR', getcwd() . '/../src/com/linways/starter/web');
// Define directory where template files resides
define('TEMPLATE_DIR', 'template');

define('TWIG_EXTENSIONS', __DIR__ . '/twigExtensions.php');

$app = new \Slim\App([
    'settings' => [
        // 'displayErrorDetails' => true,
        'debug' => getenv('DEBUG') === "true",   // change to false in production
        'addContentLengthHeader' => false,
        'determineRouteBeforeAppMiddleware' => true,
    ]
]); // change to false for production

$container = $app->getContainer();

$container['errorHandler'] = function ($container) {
    return new com\linways\exat\errorHandler\CustomErrorHandler();
};

require_once __DIR__ . '/middlewares.php';
require_once __DIR__ . '/controllers.php';

require_once __DIR__ . '/routes.php';
