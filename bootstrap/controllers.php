<?php
use com\linways\starter\web\controller\TodoController;
use com\linways\starter\web\controller\AccountsController;

$container['AccountsController'] = function ($container) {
    return new AccountsController($container);

};


$container['TodoController'] = function ($container) {
    return new TodoController($container);

};
