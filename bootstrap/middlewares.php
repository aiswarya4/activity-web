<?php
/*
 * $app and $container are available.
 */
$app->add(new \Linways\Slim\linways\slim\middleware\AddContentEncodingHeader($app));
$app->add(new \Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware($app));
