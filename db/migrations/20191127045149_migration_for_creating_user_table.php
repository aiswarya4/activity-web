<?php


use Phinx\Migration\AbstractMigration;

class MigrationForCreatingUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
      // write your SQL inside the double quotes
      $this->execute("
            CREATE TABLE `user` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `username` VARCHAR(45) NULL,
              `dob` DATE NULL,
              `email` VARCHAR(45) NULL,
              `created_by` INT NULL,
              `created_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
              `updated_by` INT NULL,
              `updated_date` DATETIME DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(),
              PRIMARY KEY (`id`),
              UNIQUE INDEX `username_UNIQUE` (`username` ASC));

      ");
    }
}
