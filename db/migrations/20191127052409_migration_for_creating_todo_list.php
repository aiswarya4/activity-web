<?php


use Phinx\Migration\AbstractMigration;

class MigrationForCreatingTodoList extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
      // write your SQL inside the double quotes
      $this->execute("
                CREATE TABLE `todo_list` (
                  `id` INT NOT NULL AUTO_INCREMENT,
                  `name` MEDIUMTEXT NULL,
                  `status` VARCHAR(45) NULL,
                  `user_id` INT NULL,
                  PRIMARY KEY (`id`),
                  INDEX `fk_todolist_user_idx` (`user_id` ASC),
                  CONSTRAINT `fk_todolist_user`
                    FOREIGN KEY (`user_id`)
                    REFERENCES `user` (`id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION);


      ");
    }
}
