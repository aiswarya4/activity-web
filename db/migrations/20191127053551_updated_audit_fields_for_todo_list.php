<?php


use Phinx\Migration\AbstractMigration;

class UpdatedAuditFieldsForTodoList extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
      // write your SQL inside the double quotes
      $this->execute("
            ALTER TABLE `todo_list` 
                ADD COLUMN `created_by` INT NULL AFTER `user_id`,
                ADD COLUMN `created_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() AFTER `created_by`,
                ADD COLUMN `updated_by` INT NULL AFTER `created_date`,
                ADD COLUMN `updated_date` DATETIME NULL ON UPDATE CURRENT_TIMESTAMP() AFTER `updated_by`;


      ");
    }
}
