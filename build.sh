#!/bin/bash
echo "########################################################### Starting build tool ##################################################"
 
RED='\033[0;31m'
GREEN='\033[0;32m' # No Color
BLUE='\e[34m'
NC='\033[0m' # No Color
ERROR='\e[101m'
SUCCESS='\e[30;48;5;82m'
BLINK=\033[5m
printf "\n What should i do for you?"
printf "\n"
printf " \n ${BLUE} 1. git-update"
printf " \n ${BLUE} 2. composer-build"
printf " \n ${BLUE} 3. db-migration"
printf " \n ${BLUE} 4. memcache-restart"
printf " \n ${BLUE} 5. deploy ( includes git update,composer-build,db-migration and restart memcache server)"
printf "\n"
printf "\n ${NC}Please choose any one of the above option to continue: "

read action


doGitupdate()
{
	printf " \n Thank you!.We are processing your request ${RED}'git-update'"
       printf "${NC}\n"
	printf "\n Your Current branch : ${GREEN}" 
	git branch | grep \* | cut -d ' ' -f2

	printf "\n${NC} Please enter branch name to continue : "
	read branch

	printf "\n Starting git pull request for the branch '"$branch"'"

	printf "\n"
	if git pull origin $branch
	 then 
	   printf "\n"	
	   printf " \n ${SUCCESS}Successfully completed git pull request for the branch :'"$branch"'"
	printf "${NC}\n"

	else
	   printf "\n ${ERROR}Error : Failed to complete  git pull request for the branch '"$branch"'.Please see the error log for more details.${NC}"
	printf "${NC}\n"
	fi
	printf "${NC}\n"

}

doComposerBuild()
{
printf "${NC} \n Thank you!.We are processing your request ${RED}'composer-build'"
	printf "${NC}\n"
	printf "\n Starting composer update"
	printf "\n"
	
	if composer update 
	  then 
	     printf "\n${GREEN} Successfully completed composer update."
	      printf "${NC}\n"
	     printf "\n Starting composer dump autoload "
	     printf "\n"
	      if composer dump-autoload -o
		then 
		   printf "\n${GREEN} Successfully completed composer dump autoload"
		   printf "${NC}\n"
		   printf "\n${SUCCESS} Successfully completed composer build"
		   printf "${NC}\n"
                   printf "\n"
 		else
		   printf "\n ${ERROR}Error : Failed to complete dump autoload.Please see error log for more details.${NC}"	
	      fi	   	
          else
             printf "\n ${ERROR}Error : Failed to complete composer update.Please see error log for more details.${NC}"
        fi
printf " ${NC}\n"

}

domemcacheRestart()
{
printf "${NC} \n Thank you!.We are processing your request ${RED}'memcache-restart'"
	printf "${NC}\n"
	printf "\n"
	printf "${NC}Starting memcache-restart"
	printf "\n"
	sudo service memcached restart
	printf "\n ${SUCCESS}Successfully completed 'Restarting memcache server'"
	printf "${NC}\n"
	printf "${NC}\n"

}

doDeploy()
{
printf "${NC} \n Thank you!.We are processing your request ${RED}'deploy'"
     	printf "${NC}\n"
	printf "\n"
	printf "${NC}Starting git-update"
	printf "${NC}\n"
	printf "\n Your Current branch : ${GREEN}" 
	git branch | grep \* | cut -d ' ' -f2

	printf "\n${NC} Please enter branch name to continue : "
	read branch

	printf "\n Starting git pull request for the branch '"$branch"'"

	printf "\n"
	if git pull origin $branch
	 then 
	   	printf "\n"	
	   	printf " \n ${SUCCESS}Successfully completed git pull request for the branch :'"$branch"'"
		printf "${NC}\n"
		printf "\n"
		printf "\n"
		printf "\n Starting composer update"
		printf "\n"
	
		if composer update 
		  then 
		     printf "\n"
		     printf "\n${GREEN} Successfully completed composer update."
		     printf "${NC}\n"
		     printf "\n Starting composer dump autoload "
		     printf "\n"
		      if composer dump-autoload -o
			then 
			   printf "\n"	
			   printf "\n${GREEN} Successfully completed composer dump autoload"
			   printf "${NC}\n"
			   printf "\n"
			   printf "\n${SUCCESS} Successfully completed composer build"
			   printf "${NC}\n"
			   printf "\n"	

              if doDbMigration
              then
                  printf "\n"	
			      printf "\n${SUCCESS} Completed 'db-migration' "
                  printf "${NC}\n"
			      printf "\n"	
			      printf "\n ${NC}Restarting memcache server"
	 		      printf "\n"
			     	sudo service memcached restart
	 		   printf "\n ${GREEN}Successfully completed 'Restarting memcache server'"
			   printf "${NC}\n"
			   printf "${NC}\n"
			   printf "${NC}\n"
			   printf " \n ${SUCCESS} Successfully completed deploy operation"
			   printf "${NC}\n"
			   printf "\n ${RED}FYI: Please refresh the cache if required(see refresh-cash.sh ).Only applicable in production"
			   printf "\n"	
              else
               printf "\n ${ERROR}Error : Failed to complete 'db-migration'.Please see error log for more details.${NC}"
               fi

	 		else
			   printf "\n ${ERROR}Error : Failed to complete dump autoload.Please see error log for more details."	
		      fi	   	
		  else
		     printf "\n ${ERROR}Error : Failed to complete composer update.Please see error log for more details.${NC}"
		fi

	else
	   printf "\n ${ERROR}Error : Failed to complete  git pull request for the branch  ".$branch."${NC}\n"
	fi
	  
printf "${NC}\n"


}

showDbMigrationFailedStatus()
{
    printf "\n"
	printf " \n${ERROR}  $1 "
	printf "\n"
	printf "${NC}\n"
}
showDbMigrationSuccessStatus()
{

	printf "\n"
	printf "\n ${SUCCESS}  $1 "
	printf "${NC}\n"
}
doDbMigration()
{
    printf "${NC}\n"
    printf "${BLUE} \n Starting ${RED}'db-migration'"
	printf "${NC}\n"
	printf "\n"
	printf "${NC}What do you want to do with the db-migration ?"
	printf "\n"
	printf " \n ${BLUE} 1. Run migrations in all colleges using nucleus."
	printf " \n ${BLUE} 2. Run migrations in a specific database."
	printf " \n ${BLUE} 3. Run migrations for a college with code."
	printf " \n ${BLUE} 4. Run migrations till a particular migration"
	printf " \n ${BLUE} 5. Fake migration in a specific database"
	printf " \n ${BLUE} 6. Fake migration for a college with code"
	printf " \n ${BLUE} 7. Fake migrations in all colleges using nucleus"
    printf "\n"
    printf "\n ${NC}Please choose any one of the above option to continue: "
    read dbmigrateaction


	case $dbmigrateaction in 
	1)
	  printf "${NC}\n"
	  printf "\n Running migrations in all colleges using nucleus." 
	  printf "\n"
	   if ./lcli db:migrate --all
	    then 
	     showDbMigrationSuccessStatus " Successfully completed 'Run migrations in all colleges using nucleus'."
	     else
	      showDbMigrationFailedStatus "'Run migrations in all colleges using nucleus' is failed.Please correct the error and try again!."
	   fi
	;;
	
	2)
	  printf "${NC}\n"
	  printf " \n ${BLUE} Starting Run migrations in a specific database."
	  printf "\n Please enter database name:" 
	  read dbName
	  
	   printf "\n Please enter database username:" 
	   read dbUser
	 
	  
	   if  ./lcli db:migrate -d $dbName -u $dbUser -p
	    then 
	     showDbMigrationSuccessStatus " Completed 'Run migrations in a specific database'."
	     else
	      showDbMigrationFailedStatus "'Run migrations in all colleges using nucleus' is failed.Please correct the error and try again!."
	      fi
	;;	
	3)
	  printf "${NC}\n"
	  printf " \n ${BLUE} Starting Run migrations for a college with code using nucleus."
	  printf "\n Please enter college code:" 
	  read collegeCode
	  
	     if  ./lcli db:migrate $collegeCode
	    then 
	     showDbMigrationSuccessStatus " Successfully completed 'Run migrations for a college with code'."
	     else
	      showDbMigrationFailedStatus "'Run migrations for a college with code' is failed.Please correct the error and try again!."
	      fi
	  
	  
	;;
	4)
	  printf "${NC}\n"
	  printf " \n ${BLUE} Starting Run migrations till a particular migration using nucleus"
	  printf "\n Please enter migration name:" 
	  read migrationName
	  
	     if  ./lcli db:migrate --all --target=$migrationName
	    then 
	     showDbMigrationSuccessStatus " Successfully completed 'Run migrations till a particular migration'."
	     else
	      showDbMigrationFailedStatus "'Run migrations till a particular migration' is failed.Please correct the error and try again!."
	      fi
	  
	;;
	5)
	  printf "${NC}\n"
	  printf " \n ${BLUE} Starting Fake migration in a specific database"
	  printf "\n Please enter db name:" 
	  read dbName
	  
	   printf "\n Please enter database username:" 
	   read dbUser
	  printf "\n Please enter migration name:" 
	  read migrationName
	  
	    if  ./lcli db:migrate-fake $migrationName -d $dbName -u $dbUser -p
	    then 
	     showDbMigrationSuccessStatus " Successfully completed 'Fake migration in a specific database'."
	     else
	      showDbMigrationFailedStatus "'Fake migration in a specific database' is failed.Please correct the error and try again!."
	      fi
	  

	;;
	6)
	  printf "${NC}\n"
	  printf " \n ${BLUE} Starting Fake migration for a college with code  using nucleus"
	  printf "\n Please enter college code:" 
	  read collegeCode
	  printf "\n Please enter migration name:" 
	  read migrationName
	  
	     if  ./lcli db:migrate-fake $migrationName $collegeCode
	    then 
	     showDbMigrationSuccessStatus " Successfully completed 'Fake migration for a college with code'."
	     else
	      showDbMigrationFailedStatus "'Fake migration for a college with code' is failed.Please correct the error and try again!."
	      fi
	  
	;;
	7)
	  printf "${NC}\n"
	  printf " \n ${BLUE} Starting Fake migrations in all colleges using nucleus"
	  printf "\n Please enter db name:" 
	  read dbName
	  printf "\n Please enter migration name:" 
	  read migrationName
	  
	  
	     if   ./lcli db:migrate-fake $migrationName --all
	    then 
	     showDbMigrationSuccessStatus " Successfully completed 'Fake migrations in all colleges using nucleus'."
	     else
	      showDbMigrationFailedStatus "'Fake migrations in all colleges using nucleus' is failed.Please correct the error and try again!."
	      fi
	 
	;;
	*)
	printf " \n${ERROR} Sorry ! Invalid option.Please choose valid option and try again!."
	printf "${NC}\n"
	;;

esac


}

case $action in 
1)
doGitupdate
;;

git-update)
doGitupdate
       
;;
2)
doComposerBuild
;;	
composer-build)
doComposerBuild
 ;;
3)
doDbMigration
;;
db-migration)
doDbMigration	
       
;;
4)
domemcacheRestart
;;
memcache-restart)
domemcacheRestart	
;;
5)
doDeploy
;;
deploy)
	doDeploy
;;

*)
printf " \n${ERROR} Sorry ! Invalid option.Please choose valid option and try again!."
printf "${NC}\n"
;;


	
esac

