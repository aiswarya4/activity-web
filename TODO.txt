1. Rename `src/com/linways/starter` to src/com/linways/<project-name>`
2. Change line 8 on `bootstrap/app.php` from `define('SOURCE_DIR', getcwd() . '/../src/com/linways/starter/web');` 
    to define('SOURCE_DIR', getcwd() . '/../src/com/linways/<project-name>/web');
3. Implement `userHasPermission` twig function if required in `bootstrap/twigExtensions.php`
4. Change namespace `com\linways\starter\web\common` in `src/com/linways/starter/web/common/BaseController.php` to 
   `com\linways\<project-name>\web\common`
5. Change autoload-psr4 in composer.json from
"autoload": {
    "psr-4": {
      "com\\linways\\starter\\": "src/com/linways/starter"
    }
  }

  to

  "autoload": {
      "psr-4": {
        "com\\linways\\<project-name>\\": "src/com/linways/<project-name>"
      }
    }

6. Delete all unnecessary file for rendering HelloWorld