<?php
namespace com\linways\starter\web\common;

use Linways\Slim\Controller\BaseController as BC;

class BaseController extends BC
{

    // change this to true for checking the permissions
    public $isPermissionsRequired = false;

     public function __construct(){
         global $container;
         parent::__construct($container);
//          session_start();
//         // Converting staffId, adminId or studentId into a common variable
//         if(isset($_SESSION['staffID'])){
//           $GLOBALS['userId'] = $_SESSION['staffID'];
//           $GLOBALS['userType'] = "STAFF";
//         }
//         else{
//             $requestURI = $_SERVER["REQUEST_URI"];
//             header("Location: /staff/index.php?next=$requestURI");exit();
//         }

        $requestURI = $_SERVER["REQUEST_URI"];
   
        if(!isset($_SESSION['userId'])){
          if(!($requestURI == '/accounts/signin' || $requestURI == '/accounts/signin/' || $requestURI == '/accounts/signup' || $requestURI == '/accounts/signup/' )){
            header("Location: /accounts/signin");exit();

          }

        }
     }


    public function hasPermission($permissions){
    // return PermissionService::getInstance()->staffHasPermission($_SESSION['staffID'], $permissions);

      // Handling Roles
      // $roles = $permissions['roles'];
      // $roles = array_map('strtoupper', $roles);
      // return in_array($GLOBALS['userType'], $roles);
      return true;
    }
}
