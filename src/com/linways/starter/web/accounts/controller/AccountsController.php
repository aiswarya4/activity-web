<?php
namespace com\linways\starter\web\controller;

use Slim\Http\Request;
use Slim\Http\Response;
use Linways\Slim\Utils\ResponseUtils;
use com\linways\core\starter\dto\User;
use com\linways\core\starter\service\UserService;
use com\linways\starter\web\common\BaseController;
use Respect\Validation\Exceptions\ValidationException;
use com\linways\core\starter\exception\ActivityException;
use SebastianBergmann\Environment\Console;


/**
 * functions that handled by the accounts controller defined in this class
 */
class AccountsController extends BaseController
{
    /**
     * register new users
     *
     * @param Request $request
     * @param Response $response
     * @return void
     */
    protected function register(Request $request, Response $response)
    {
      if ($request->isGet())
        return ResponseUtils::renderToResponse($response, 'accounts/registration.twig',[ ]);
      elseif ($request->isPost()){
        $user = new User();
        $user->userName = $request->getParsedBodyParam('username');
        $user->dob = $request->getParsedBodyParam('dob');
        $user->email = $request->getParsedBodyParam('email');
        $user->password = $request->getParsedBodyParam('password');
        try{
          $user = UserService::getInstance()->createUser($user);
          return ResponseUtils::result($response, $user);
        }catch (\Exception $e){
          $message = "User Registration failed. Please try again.";
          if($e->getCode()== ActivityException::DUPLICATE_ENTRY){
              $message = "Already registered username";
          }
          return ResponseUtils::fault($response, $message);
        }

      }
        
    }

    /**
     * registered users login to their account using email id and password
     *
     * @param Request $request
     * @param Response $response
     * @return void
     */
    protected function login(Request $request, Response $response)
    {
      if ($request->isGet())
        return ResponseUtils::renderToResponse($response, 'accounts/login.twig',[
            ]);
      elseif ($request->isPost()){
        $email = $request->getParsedBodyParam('email');
        $password = $request->getParsedBodyParam('password');
        try{
            $user = UserService::getInstance()->getUserDetailsByEmailAndPassword($email,$password);
          //   console.log('$user');

            if(empty($user)){
              return ResponseUtils::fault($response, "User login failed");
            }
            $_SESSION['userId'] = $user->id;
            return ResponseUtils::result($response, $user);
        }catch (\Exception $e){
          return ResponseUtils::fault($response, "User login failed");
        }
  
      }
    }
}


