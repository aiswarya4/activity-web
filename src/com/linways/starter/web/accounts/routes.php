<?php
$app->map(['GET', 'POST'],'/signup[/]', 'AccountsController:register')->setName("accounts:registration");
$app->map(['GET', 'POST'],'/signin[/]', 'AccountsController:login')->setName("accounts:login");

