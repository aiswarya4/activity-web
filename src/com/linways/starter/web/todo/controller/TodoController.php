<?php
namespace com\linways\starter\web\controller;

use Slim\Http\Request;
use Slim\Http\Response;
use Linways\Slim\Utils\ResponseUtils;
use com\linways\core\starter\dto\User;
use com\linways\core\starter\service\TodoService;
use com\linways\core\starter\service\UserService;
use com\linways\starter\web\common\BaseController;
use Respect\Validation\Exceptions\ValidationException;
use com\linways\core\starter\request\GetTodoListRequest;
use com\linways\core\starter\exception\ActivityException;

/**
 * Define todo services
 */
class TodoController extends BaseController
{
  /**
   *display todo home page 
   *
   * @param Request $request
   * @param Response $response
   * @return void
   */  
  protected function Todo(Request $request, Response $response)
  {
    if ($request->isGet())
        return ResponseUtils::renderToResponse($response, 'todo/home.twig',[ ]);
    elseif ($request->isPost()){
    }
  }
  
  /**
   *display TODO list
   *
   * @param Request $request
   * @param Response $response
   * @return void
   */
  protected function renderTodoList(Request $request, Response $response)
  {
    $todoList = NULL;
    if ($request->isGet())
        return ResponseUtils::renderToResponse($response, 'todo/home.twig',[ ]);
    elseif ($request->isPost())
    {
        $todoRequest = new GetTodoListRequest;
        $todoRequest->userId = $_SESSION['userId'];
        
        try{
            $todoList = TodoService::getInstance()->getTodoList($todoRequest);
            if(empty($todoList)){
                return ResponseUtils::fault($response, "No Recodrs");
            }
            $html = ResponseUtils::render('todo/components/list.twig', ["todos" =>$todoList]);
            return ResponseUtils::result($response, ["html" => $html]);

        }catch (\Exception $e){
            return ResponseUtils::fault($response, "Error");
        }
    }
  }

}