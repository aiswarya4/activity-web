<?php
$app->map(['GET', 'POST'],'/home', 'TodoController:Todo')->setName("todo:home");
$app->map(['GET', 'POST'],'/list', 'TodoController:renderTodoList')->setName("todo:list");
$app->map(['GET', 'POST'],'/create', 'TodoController:createTodo')->setName("todo:create");
