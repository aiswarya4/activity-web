<?php
/**
 * User: jithinvijayan
 * Date: 22/04/18
 * Time: 8:31 AM
 */

namespace com\linways\exat\utils;

use Slim\Http\Response;

class CommonUtil
{
    /**
     * Method for handling Controller result.
     * <p> Parse controller success response to ajax. </p>
     * @param Response $response
     * @param unknown $data
     * @return \Slim\Http\Response
     */
    public static function result(Response $response, $data = null)
    {
        $ajaxResponse = new \stdClass();
        $ajaxResponse->status = "success";
        $ajaxResponse->data = $data;
        return $response->withJson($ajaxResponse, 200);
    }

    /**
     * Method for handling controller fault/error.
     * <p> Parse controller fault/error response to ajax. </p>
     * @param Response $response
     * @param \Exception $exception
     * @return \Slim\Http\Response
     */
    public static function fault(Response $response, $exception = null)
    {
        $ajaxResponse = new \stdClass();
        $ajaxResponse->status = "error";

        if (!empty($exception) && $exception instanceof \Exception) {
            $ajaxResponse->code = $exception->getCode();
            $ajaxResponse->message = $exception->getMessage();
        } else {
            $ajaxResponse->message = $exception;
        }
        return $response->withJson($ajaxResponse, 200);
    }

}