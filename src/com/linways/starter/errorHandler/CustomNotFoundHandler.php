<?php
namespace com\linways\fee\errorHandler;

use Linways\Slim\Utils\ResponseUtils;
use Slim\Http\Request;
use Slim\Http\Response;
use \Linways\Slim\Utils\MessageUtils;
use \Linways\Slim\Exception\CoreException;
/**
 * Class CustomNotFoundHandler  - Shows proper 404 error page
 * @package com\linways\fee\errorHandler
 */
class CustomNotFoundHandler
{

    public function __invoke(Request $request, Response $response, $exception)
    {
            return ResponseUtils::renderToResponse($response, "common/pages/error404.twig", []);
    }
}
