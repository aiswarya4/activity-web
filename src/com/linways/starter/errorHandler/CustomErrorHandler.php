<?php
namespace com\linways\exat\errorHandler;

use Linways\Slim\Utils\ResponseUtils;
use Slim\Http\Request;
use Slim\Http\Response;
use \Linways\Slim\Utils\MessageUtils;
use \Linways\Slim\Exception\CoreException;
/**
 * Class CustomErrorHandler  - Handle exceptions and error in production mode (Debug=false)
 * @package com\linways\exat\middleware
 */
class CustomErrorHandler{

    public function __invoke(Request $request, Response $response, $exception) {
        if (method_exists($exception, 'getErrorCode')) {
            $errorCode = $exception->getErrorCode();
        }elseif (method_exists($exception, 'getCode')) {
            $errorCode = $exception->getCode();
        }
        else {
            $errorCode = CoreException::INTERNAL_SERVICE_FAILURE;
        }
        if($errorCode === CoreException::PERMISSION_DENIED){
            return ResponseUtils::renderToResponse($response, "common/pages/error401.twig" , []);
        }
        return MessageUtils::showError($response, $errorCode, $exception->getMessage());
    }
}
